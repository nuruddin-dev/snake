using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MenuScene
{
    public class MenuManager : MonoBehaviour
    {
        public GameObject startPanel;
        public GameObject settingPanel;
        public TextMeshProUGUI highScoreText;
        
        // Reference to the Toggle components
        public Toggle soundToggle;
        public Toggle backgroundMusicToggle;
        // Reference to the Slider component
        public Slider movementSpeedSlider;
        
        private float _sliderValue;
        private float _movementSpeed;
        private int _borderType;
        private bool _sound;
        private bool _backgroundMusic;    
        private int _highScore;
        
        private const float MinMovementSpeed = 0.07f;
        private const float MaxMovementSpeed = 0.03f;
        private const float MinSliderValue = 0.1f;
        private const float MaxSliderValue = 1.1f;
        
        public TMP_Dropdown dropdown;

        [SerializeField] private AudioSource backgroundMusicAudioSource;
        [SerializeField] private AudioSource toggleAudioSource;

        // Start is called before the first frame update
        private void Start()
        {
            InitializeGameData();
            PlayBackgroundMusic();
            ManagePanel(true, false);
            ShowHighScore();
        }

        private void InitializeGameData()
        {
            var gameData = Utils.GetGameData();
            _borderType = gameData.borderType;
            _movementSpeed = gameData.movementSpeed;
            _highScore = gameData.highScore;
            _sound = gameData.sound;
            _backgroundMusic = gameData.backgroundMusic;
        }

        private void PlayBackgroundMusic()
        {
            if (_backgroundMusic)
                backgroundMusicAudioSource.Play();
        }

        private void ManagePanel(bool startPanelVisibility, bool settingPanelVisibility)
        {
            startPanel.SetActive(startPanelVisibility);
            settingPanel.SetActive(settingPanelVisibility);
        }

        private void ShowHighScore()
        {
            highScoreText.text = StringData.HighScoreText + _highScore;
        }

        // It will handle all button clicked from the UI
        public void HandleButtonClick(string buttonName)
        {
            switch (buttonName)
            {
                case StringData.PlayButton: 
                    PlayGame();
                    break;
                case StringData.SettingButton:
                    Setting();
                    break;
                case StringData.BackButton:
                    BackToHome();
                    break;
            }
        }

        private void Setting()
        {
            ManagePanel(false, true);
            
            soundToggle.isOn = _sound;
            soundToggle.onValueChanged.AddListener(OnSoundToggleValueChanged);
            
            backgroundMusicToggle.isOn = _backgroundMusic;
            backgroundMusicToggle.onValueChanged.AddListener(OnBackgroundMusicToggleValueChanged);
            
            var borderTypes = Enum.GetNames(typeof(BorderType));
            var borderTypeList = Utils.GetBorderTypes(borderTypes);
            dropdown.AddOptions(borderTypeList);
            dropdown.value = _borderType;
            dropdown.onValueChanged.AddListener(OnDropdownValueChanged);

            movementSpeedSlider.minValue = MinSliderValue;
            movementSpeedSlider.maxValue = MaxSliderValue;
            _sliderValue = MovementSpeedToSliderValue(_movementSpeed, MinMovementSpeed, MaxMovementSpeed, MinSliderValue, MaxSliderValue);
            movementSpeedSlider.value = _sliderValue;
            movementSpeedSlider.onValueChanged.AddListener(OnSliderValueChanged);
        }

        private void OnDropdownValueChanged(int index)
        {
            _borderType = index;
        }

        // Calculate the slider value from current movement speed
        private float MovementSpeedToSliderValue(float speed, float minSpeed, float maxSpeed, float minSlider, float maxSlider)
        {
            var sliderValue = ((speed - minSpeed) / (maxSpeed - minSpeed)) * (maxSlider - minSlider) + minSlider;
            return sliderValue;
        }
        
        // Callback method for the sound Toggle's value change event
        private void OnSoundToggleValueChanged(bool isOn)
        {
            // Handle the toggle state change
            _sound = isOn;
            if(_sound) toggleAudioSource.Play();
        }
        
        // Callback method for the background music Toggle's value change event
        private void OnBackgroundMusicToggleValueChanged(bool isOn)
        {
            _backgroundMusic = isOn;
            if(_sound) toggleAudioSource.Play();
            if(!isOn) backgroundMusicAudioSource.Stop();
            else backgroundMusicAudioSource.Play();
        }

        // Callback method for the Slider's value changed event
        private void OnSliderValueChanged(float value)
        {
            // Handle the slider value change
            _sliderValue = value;
            UpdateSpeed();
        }

        // Update the movement speed from the slider value
        private void UpdateSpeed()
        {
            if (movementSpeedSlider == null) return;
            // Scale the speed based on the slider value
            var normalizedValue = movementSpeedSlider.normalizedValue;
            var scaledSpeed = Mathf.Lerp(MinMovementSpeed, MaxMovementSpeed, normalizedValue);
            _movementSpeed = scaledSpeed;
        }
        
        private static void PlayGame()
        {
            // Load the "GameScene"
            SceneManager.LoadScene(StringData.GameScene);
        }
        
        private void BackToHome()
        {
            ManagePanel(true, false);
            Utils.SetGameData(_borderType, _movementSpeed, _highScore, _sound, _backgroundMusic);
        }
    }
}
