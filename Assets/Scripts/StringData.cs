public static class StringData
{
        public const string HighScoreText = "High Score: ";
        public const string ScoreText = "Score: ";
        public const string PlayButton = "PlayButton";
        public const string SettingButton = "SettingButton";
        public const string BackButton = "BackButton";
        public const string GameScene = "GameScene";
        public const string MenuScene = "MenuScene";
        public const string BackgroundMusic = "BackgroundMusic";
        public const string Food = "Food";
        public const string GameOver = "GameOver";
        public const string HighScore = "HighScore";
        public const string Head = "Head";
        public const string BodyContainer = "BodyContainer";
        public const string Body = "Body";
        public const string Bonus = "Bonus";
        public const string BonusTimeSlider = "BonusTimeSlider";
        public const string Border = "Border";
        public const string BorderTop = "BorderTop";
        public const string BorderBottom = "BorderBottom";
        public const string BorderLeft = "BorderLeft";
        public const string BorderRight = "BorderRight";
        public const string GameField = "GameField";
        public const string RightButton = "RightButton";
        public const string LeftButton = "LeftButton";
        public const string UpButton = "UpButton";
        public const string DownButton = "DownButton";
        public const string GameData = "GameData";
}