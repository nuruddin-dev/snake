using System;

[Serializable]
public class GameDataEntity
{
    public int borderType;
    public float movementSpeed;
    public int highScore;
    public bool sound;
    public bool backgroundMusic;

    public GameDataEntity(int borderType, float movementSpeed, int highScore, bool sound, bool backgroundMusic)
    {
        this.borderType = borderType;
        this.movementSpeed = movementSpeed;
        this.highScore = highScore;
        this.sound = sound;
        this.backgroundMusic = backgroundMusic;
    }

}