using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class Utils
{
    public static GameDataEntity GetGameData()
    {
        if (!PlayerPrefs.HasKey(StringData.GameData)) 
            SetGameData(1,0.05f,0,true,true);
        
        // Retrieve the JSON string from PlayerPrefs
        var jsonData = PlayerPrefs.GetString(StringData.GameData);
        // Convert the JSON string back to GameData
        var gameData = JsonUtility.FromJson<GameDataEntity>(jsonData);
        
        return gameData;
    }

    public static void SetGameData(int gameLevel, float movementSpeed, int highScore, bool sound, bool backgroundMusic)
    {
        var gameDataEntity = new GameDataEntity
        (
            gameLevel,
            movementSpeed,
            highScore,
            sound,
            backgroundMusic
        );
            
        // Convert the GameData to a JSON string
        var jsonData = JsonUtility.ToJson(gameDataEntity);

        // Store the JSON string in PlayerPrefs (or another data management solution)
        PlayerPrefs.SetString(StringData.GameData, jsonData);
    }

    public static void SetHighScore(int highScore)
    {
        var currentGameData = GetGameData();
        var gameDataEntity = new GameDataEntity
        (
            currentGameData.borderType,
            currentGameData.movementSpeed,
            highScore,
            currentGameData.sound,
            currentGameData.backgroundMusic
        );
        var jsonData = JsonUtility.ToJson(gameDataEntity);
        PlayerPrefs.SetString(StringData.GameData, jsonData);
    }

    public static List<string> GetBorderTypes(string[] borderTypes)
    {
        var borderTypeList = new List<string>();
        foreach (var borderType in borderTypes)
        {
            StringBuilder result = new();
            result.Append(borderType[0]); // Add the first character as is

            for (var i = 1; i < borderType.Length; i++)
            {
                // Insert a space before each uppercase letter
                if (char.IsUpper(borderType[i]))
                {
                    result.Append(' '); // Insert a space
                }

                result.Append(borderType[i]); // Add the current character
            }
            borderTypeList.Add(result.ToString());
        }
        return borderTypeList;
    }
}
