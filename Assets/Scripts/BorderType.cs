public enum BorderType
{
        NoBorder,
        FourSidedWall,
        HorizontalWall,
        VerticalWall,
        ZicZacWall
}