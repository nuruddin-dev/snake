using UnityEngine;

namespace GameScene
{
    public class FoodScript : MonoBehaviour
    {
        // This is the GameManager.cs from the active GameManager GameObject. // Need to set from the GameManager.cs
        public GameManager gameManager;

        // Start is called before the first frame update
        void Start()
        {
            if (gameObject.name == StringData.Bonus)
            {
                // Invoke the DestroyObject method after the specified time
                Invoke(nameof(DestroyBonus), 5f);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag is not (StringData.Body or StringData.Border)) return;
            
            gameManager.SpawnFood(gameObject.name);
            Destroy(gameObject);
        }

        private void DestroyBonus()
        {
            // Destroy the game object
            Destroy(gameObject);
        }
    }
}
