using UnityEngine;
using UnityEngine.UI;

namespace GameScene
{
    public class BonusTimeSliderScript : MonoBehaviour
    {
        // This is the GameManager.cs from the active GameManager GameObject. // Need to set from the GameManager.cs
        public GameManager gameManager;
        
        public Slider slider;
        public float totalTime = 5f;
        private float _startTime;

        void Start()
        {
            _startTime = Time.time;

            // Fill the slider at the start
            FillSlider();
        }

        void Update()
        {
            // Calculate the elapsed time since the start
            var elapsedTime = Time.time - _startTime;

            // Calculate the fill percentage based on elapsed time
            var elapsedPercentage = Mathf.Clamp01(elapsedTime / totalTime);
            
            // Update the slider value based on the fill percentage
            slider.value = 1f - elapsedPercentage;
            var mappedBonusScore = GameManager.MapValue(slider.value, 0f, 1f, 1f, 5f);
            gameManager.bonusScore = (int) mappedBonusScore;

            // Check if slider's fill is empty that means bonus time is over or bonus is taken
            if (slider.value == 0f || gameManager.bonusEaten || gameManager.gameStatus == 0)
            {
                if (slider.value == 0f)
                {
                    if(gameManager.sound) gameManager.bonusEndSound.Play();
                }
                gameManager.bonusEaten = false;
                // Destroy the game object
                Destroy(gameObject);
            }
        }

        private void FillSlider()
        {
            // Set the slider value to 1 (full)
            slider.value = 1f;
        }
    }
}