using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace GameScene
{
    public class GameManager : MonoBehaviour
    {
        public GameObject headPrefab;
        public GameObject bodyPrefab;
        public GameObject foodPrefab;
        public GameObject bonusPrefab;
        public GameObject borderPrefab;
        public GameObject gameFieldPrefab;
        public GameObject gameOverScreen;
        public GameObject controllerButtons;
        public GameObject others;
        public GameObject bonusTimeSlider;
        private GameObject _gameField;
        private GameObject _bodyContainer;
        
        public Transform canvasTransform;
        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI totalScore;
        public RectTransform scoreRectTransform;
        private Vector3 _scoreTextWorldPosition;
        
        public float gameFieldWidth;
        public float gameFieldHeight;

        private float _mainCameraHeight;
        private float _mainCameraWidth;

        public float headSize = 2f;
        public int gameStatus;
        public int score;
        public int highScore;
        public float movementSpeed;
        public bool foodEaten;
        private readonly List<Transform> _body = new();
        public int borderType;
        public bool sound;
        public int bonusScore;
        public bool bonusEaten;
        private bool _backgroundMusic;
        
        private const float MinMovementSpeed = 0.07f;
        private const float MaxMovementSpeed = 0.03f;
        private const float MinScore = 1f;
        private const float MaxScore = 5f;

        public int scoreIncreaseValue;
        public float gameFieldShiftInY;

        [SerializeField] private AudioSource ateSound;
        [SerializeField] private AudioSource bonusStartSound;
        [SerializeField] public AudioSource bonusEndSound;
        [SerializeField] private AudioSource gameOverSound;
        [SerializeField] private AudioSource highScoreSound;
        [SerializeField] private AudioSource backgroundMusic;
        
        private HeadScript _headScript;
        
        // Start is called before the first frame update
        private void Start()
        {
            if (Camera.main == null)
            {
                ExitGame();
                return;
            }
            
            InitializeGameData();
            PlayAudio(StringData.BackgroundMusic);
            CreateGameField();
            InstantiateBodyContainer();
            CalculateScoreIncreaseValue();
            ManagePanel(true, true, false);
            
            CreateBorder();

            InstantiateSnake();
            SpawnFood(StringData.Food);

            gameStatus = 1;
            Time.timeScale = 1;
        }

        private void InitializeGameData()
        {
            var gameData = Utils.GetGameData();
            borderType = gameData.borderType;
            movementSpeed = gameData.movementSpeed;
            highScore = gameData.highScore;
            sound = gameData.sound;
            _backgroundMusic = gameData.backgroundMusic;
        }

        private void PlayAudio(string audioName)
        {
            switch (audioName)
            {
                case StringData.BackgroundMusic:
                    if(_backgroundMusic)
                        backgroundMusic.Play();
                    break;
                case StringData.GameOver:
                    if(sound)
                        gameOverSound.Play();
                    break;
                case StringData.HighScore:
                    if(sound)
                        highScoreSound.Play();
                    break;
            }
        }
        private void StopAudio(string audioName)
        {
            switch (audioName)
            {
                case StringData.BackgroundMusic:
                    if(backgroundMusic.isPlaying)
                        backgroundMusic.Stop();
                    break;
            }
        }
        
        private void CalculateScoreIncreaseValue()
        {
            scoreIncreaseValue = (int) MapValue(movementSpeed, MinMovementSpeed, MaxMovementSpeed, MinScore, MaxScore);
        }

        private void InstantiateSnake()
        {
            var gameFieldPosition = _gameField.transform.position;
            var headInstancePosition = new Vector2((int) gameFieldPosition.x, (int) gameFieldPosition.y);
            var head = InstantiatePrefab(StringData.Head, headInstancePosition, Quaternion.identity);
            head.name = StringData.Head;
            _headScript = head.GetComponent<HeadScript>();
            _headScript.gameManager = this;
            var newBody = InstantiatePrefab(StringData.Body, head.transform.position - new Vector3(headSize, 0f, 0f), Quaternion.identity);
            newBody.transform.parent = _bodyContainer.transform;
            // Keep track of it in our tail list
            _body.Insert(0, newBody.transform);
            var tail = InstantiatePrefab(StringData.Body, newBody.transform.position - new Vector3(headSize, 0f, 0f), Quaternion.identity);
            tail.transform.parent = _bodyContainer.transform;
            // Keep track of it in our tail list
            _body.Insert(1, tail.transform);
        }
        
        private void InstantiateBodyContainer()
        { 
            // Create an empty GameObject name BodyContainer
            _bodyContainer = new GameObject
            {
                name = StringData.BodyContainer
            };
        }

        private void ManagePanel(bool controllerButtonsVisibility, bool othersVisibility, bool gameOverPanelVisibility)
        {
            controllerButtons.SetActive(controllerButtonsVisibility);
            others.SetActive(othersVisibility);
            gameOverScreen.SetActive(gameOverPanelVisibility);
        }

        public void BodyManagement(Vector2 headPosition)
        {
            if (foodEaten)
            {
                var newBody = InstantiatePrefab(StringData.Body, headPosition, Quaternion.identity);
                newBody.transform.parent = _bodyContainer.transform;
                // Keep track of it in our tail list
                _body.Insert(0, newBody.transform);

                // Reset the flag
                foodEaten = false;
            }
            // Do we have a Tail?
            else if (_body.Count > 0)
            {
                // Move the last Tail Element to where the Head was
                _body.Last().position = headPosition;

                // Add to the front of the list, remove from the back
                _body.Insert(0, _body.Last());
                _body.RemoveAt(_body.Count - 1);
            }
        }

        public void SpawnFood(string foodName)
        {
            if (foodName == StringData.Bonus)
            {
                var slider = InstantiatePrefab(StringData.BonusTimeSlider, Vector3.zero, Quaternion.identity);
                // Set the parent of the instantiated object to the Canvas
                slider.transform.SetParent(canvasTransform, false);
                slider.GetComponent<BonusTimeSliderScript>().gameManager = this;
                if(sound) bonusStartSound.Play();
            }
            
            // x position between left & right border
            var x = GetRandomEvenCoordinate(-gameFieldWidth /2 + headSize , gameFieldWidth /2 - headSize);

            // y position between top & bottom border
            var y = GetRandomEvenCoordinate(-(gameFieldHeight / 2) + gameFieldShiftInY + headSize, (gameFieldHeight / 2) + gameFieldShiftInY - headSize);

            // Instantiate the food at (x, y)
            var foodPrefabName = foodName == StringData.Food ? StringData.Food : StringData.Bonus;
            var instantiatedPrefab = InstantiatePrefab(foodPrefabName, new Vector2(x, y), Quaternion.identity);
            instantiatedPrefab.name = foodPrefabName;
            instantiatedPrefab.GetComponent<FoodScript>().gameManager = this;
        }

        private float GetRandomEvenCoordinate(float min, float max)
        {
            var randomCoordinate = Random.Range(min, max);
            return Mathf.Round(randomCoordinate / 2) * headSize; // Ensure the coordinate is divisible by _headSize (2 here)
        }

        private GameObject InstantiatePrefab(string prefabName, Vector3 position, Quaternion rotation)
        {
            var newGameObject = prefabName switch
            {
                StringData.Head => Instantiate(headPrefab, position, rotation),
                StringData.Body => Instantiate(bodyPrefab, position, rotation),
                StringData.Food => Instantiate(foodPrefab, position, rotation),
                StringData.Bonus => Instantiate(bonusPrefab, position, rotation),
                StringData.Border => Instantiate(borderPrefab, position, rotation),
                StringData.GameField => Instantiate(gameFieldPrefab, position, rotation),
                StringData.BonusTimeSlider => Instantiate(bonusTimeSlider, bonusTimeSlider.transform.position, rotation),
                _ => null
            };

            return newGameObject;
        }

        public void GameOver()
        {
            gameStatus = 0;
            PlayAudio(StringData.GameOver);
            StopAudio(StringData.BackgroundMusic);
            ManagePanel(false, false, true);
            totalScore.text = StringData.ScoreText + score;
            if (score <= highScore) return;
            PlayAudio(StringData.HighScore);
            // Create a new instance of GameData and set its values
            Utils.SetHighScore(score);
        }

        public void GoToHome()
        {
            // Load the "MenuScene"
            SceneManager.LoadScene(StringData.MenuScene);
        }
        
        // When exit button clicked on start or setting screen
        public void ExitGame()
        {
            #if UNITY_EDITOR
                        // If running in the Unity Editor, stop playing
                        UnityEditor.EditorApplication.isPlaying = false;
            #else
                        // If running as a standalone build, quit the application
                        Application.Quit();
            #endif
        }

        public void ReplayGame()
        {
            var currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
            Time.timeScale = 1;
        }
        
        public static float MapValue(float value, float originalMin, float originalMax, float newMin, float newMax)
        {
            // Ensure value is within the valid range
            value = Mathf.Clamp(value, originalMin, originalMax);

            // Map value to the value range using a linear mapping formula
            // It will output the value [0,1]
            var normalizedValue = (value - originalMin) / (originalMax - originalMin);
            // Map the value in between the range given
            var mappedValue = Mathf.Lerp(newMin, newMax + 1, normalizedValue);

            return mappedValue;
        }
        
        public void UpdateScore()
        {
            if(sound) ateSound.Play();
            scoreText.text = StringData.ScoreText + score;
        }
        
        private void CreateBorder()
        {
            var borderTypes = (BorderType[])Enum.GetValues(typeof(BorderType));

            if(borderType == Array.IndexOf(borderTypes, BorderType.NoBorder))
                return;

            if (borderType == Array.IndexOf(borderTypes, BorderType.FourSidedWall))
                CreateFourSidedWall();
            
            if (borderType == Array.IndexOf(borderTypes, BorderType.HorizontalWall))
                CreateHorizontalWall();
            
            if (borderType == Array.IndexOf(borderTypes, BorderType.VerticalWall))
                CreateVerticalWall();
            
            if (borderType == Array.IndexOf(borderTypes, BorderType.ZicZacWall))
                CreateZicZacWall();
        }

        private void CreateZicZacWall()
        {
            var gameFieldScale = _gameField.transform.localScale;
            var gameFieldPosition = _gameField.transform.position;
            
            var borderTop = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderBottom = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderLeft = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderRight = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            
            borderTop.name = StringData.BorderTop;
            borderTop.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderTop.transform.localScale = new Vector2(_mainCameraWidth/2, 1f);
            borderTop.transform.position = new Vector2(_mainCameraWidth/4f, gameFieldPosition.y + gameFieldScale.y/8f - 0.5f);
            
            borderBottom.name = StringData.BorderBottom;
            borderBottom.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderBottom.transform.localScale = new Vector2(_mainCameraWidth/2, 1f);
            borderBottom.transform.position = new Vector2(-_mainCameraWidth/4f, gameFieldPosition.y - gameFieldScale.y/8f + 0.5f);
            
            borderLeft.name = StringData.BorderLeft;
            borderLeft.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderLeft.transform.localScale = new Vector2(1f, gameFieldScale.y/2);
            borderLeft.transform.position = new Vector2( -_mainCameraWidth/4f + 0.5f, gameFieldPosition.y + gameFieldScale.y/4f - 0.5f);
            
            borderRight.name = StringData.BorderRight;
            borderRight.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderRight.transform.localScale = new Vector2(1f, gameFieldScale.y/2);
            borderRight.transform.position = new Vector2( _mainCameraWidth/4f - 0.5f, gameFieldPosition.y - gameFieldScale.y/4f + 0.5f);
        }

        private void CreateVerticalWall()
        {
            var gameFieldScale = _gameField.transform.localScale;
            var gameFieldPosition = _gameField.transform.position;
            
            var borderLeft = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderRight = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            
            borderLeft.name = StringData.BorderLeft;
            borderLeft.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderLeft.transform.localScale = new Vector2(1f, gameFieldScale.y/2);
            borderLeft.transform.position = new Vector2( -_mainCameraWidth/4f + 0.5f, gameFieldPosition.y);
            
            borderRight.name = StringData.BorderRight;
            borderRight.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderRight.transform.localScale = new Vector2(1f, gameFieldScale.y/2);
            borderRight.transform.position = new Vector2( _mainCameraWidth/4f - 0.5f, gameFieldPosition.y);
        }

        private void CreateHorizontalWall()
        {
            var gameFieldScale = _gameField.transform.localScale;
            var gameFieldPosition = _gameField.transform.position;
            
            var borderTop = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderBottom = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            
            borderTop.name = StringData.BorderTop;
            borderTop.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderTop.transform.localScale = new Vector2(_mainCameraWidth/2, 1f);
            borderTop.transform.position = new Vector2(0f, gameFieldPosition.y + gameFieldScale.y/4f - 0.5f);
            
            borderBottom.name = StringData.BorderBottom;
            borderBottom.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderBottom.transform.localScale = new Vector2(_mainCameraWidth/2, 1f);
            borderBottom.transform.position = new Vector2(0f, gameFieldPosition.y - gameFieldScale.y/4f + 0.5f);

        }

        private void CreateFourSidedWall()
        {
            var gameFieldScale = _gameField.transform.localScale;
            var gameFieldPosition = _gameField.transform.position;
            
            var borderTop = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderBottom = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderLeft = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            var borderRight = InstantiatePrefab(StringData.Border, Vector3.zero, Quaternion.identity);
            
            borderTop.name = StringData.BorderTop;
            borderTop.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderTop.transform.localScale = new Vector2(_mainCameraWidth, 1f);
            borderTop.transform.position = new Vector2(0f, gameFieldPosition.y + gameFieldScale.y/2f - 0.5f);
            
            borderBottom.name = StringData.BorderBottom;
            borderBottom.GetComponent<BoxCollider2D>().size = new Vector2(1f, 2f);
            borderBottom.transform.localScale = new Vector2(_mainCameraWidth, 1f);
            borderBottom.transform.position = new Vector2(0f, gameFieldPosition.y - gameFieldScale.y/2f + 0.5f);
            
            var topY = borderTop.transform.position.y;
            var bottomY = borderBottom.transform.position.y;
            var leftRightY = (topY + bottomY) / 2;
            
            borderLeft.name = StringData.BorderLeft;
            borderLeft.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderLeft.transform.localScale = new Vector2(1f, gameFieldScale.y);
            borderLeft.transform.position = new Vector2( -_mainCameraWidth/2f + 0.5f, leftRightY);
            
            borderRight.name = StringData.BorderRight;
            borderRight.GetComponent<BoxCollider2D>().size = new Vector2(2f, 1f);
            borderRight.transform.localScale = new Vector2(1f, gameFieldScale.y);
            borderRight.transform.position = new Vector2( _mainCameraWidth/2f - 0.5f, leftRightY);
        }

        private void CreateGameField()
        {
            var mainCamera = Camera.main!;

            var orthographicSize = mainCamera.orthographicSize;
            _mainCameraHeight = orthographicSize * 2f;
            _mainCameraWidth = orthographicSize * 2f * mainCamera.aspect;
            
            // Get the screen position of the bottom center of the ScoreText
            var scoreRectTransformPosition = scoreRectTransform.position;
            var scoreTextScreenPoint = new Vector2(scoreRectTransformPosition.x, scoreRectTransformPosition.y);

            // Convert the screen position to a world position
            _scoreTextWorldPosition = mainCamera.ScreenToWorldPoint(scoreTextScreenPoint);
            
            _gameField = InstantiatePrefab(StringData.GameField, Vector3.zero, Quaternion.identity);
            _gameField.name = StringData.GameField;
            _gameField.transform.localScale = new Vector2(_mainCameraWidth + 1, _mainCameraHeight/2 + 1);
            var gameFieldTopY = (_mainCameraHeight / 2 - _scoreTextWorldPosition.y) * 2f;
            gameFieldShiftInY = _mainCameraHeight / 4 - gameFieldTopY;
            _gameField.transform.position = new Vector2(0f, gameFieldShiftInY);
            var gameFieldSize = _gameField.GetComponent<SpriteRenderer>().bounds.size;
            gameFieldHeight = gameFieldSize.y;
            gameFieldWidth = gameFieldSize.x;
        }
        
        public void ControllerButtonClicked(string buttonName)
        {
            switch (buttonName)
            {
                case StringData.RightButton:
                    RightButtonClicked();
                    break;
                case StringData.LeftButton: 
                    LeftButtonClicked();
                    break;
                case StringData.UpButton: 
                    UpButtonClicked();
                    break;
                case StringData.DownButton: 
                    DownButtonClicked();
                    break;
            }
        }

        private void RightButtonClicked()
        {
            if (_headScript.movementDirection == Vector2.left || _headScript.movementDirection == Vector2.right)
                return;
            _headScript.movementDirection = Vector2.right;
        }

        private void LeftButtonClicked()
        {
            if (_headScript.movementDirection == Vector2.left || _headScript.movementDirection == Vector2.right)
                return;
            _headScript.movementDirection = Vector2.left;
        }

        private void UpButtonClicked()
        {
            if(_headScript.movementDirection == Vector2.up || _headScript.movementDirection == Vector2.down) 
                return;
            _headScript.movementDirection = Vector2.up;
        }

        private void DownButtonClicked()
        {
            if(_headScript.movementDirection == Vector2.up || _headScript.movementDirection == Vector2.down) 
                return;
            _headScript.movementDirection = Vector2.down;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SpawnFood(StringData.Food);
            }
        }
    }
}
