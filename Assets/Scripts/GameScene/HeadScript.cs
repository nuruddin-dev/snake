using System.Collections;
using UnityEngine;

namespace GameScene
{
    public class HeadScript : MonoBehaviour
    {
        // This is the GameManager.cs from the active GameManager GameObject. // Need to set from the GameManager.cs
        public GameManager gameManager;
        
        public Vector2 movementDirection;
        
        private float _gameFieldWidth;
        private float _gameFieldHeight;
        private float _gameFieldShiftInY;
        private float _foodCount;
        
        // Start is called before the first frame update
        void Start()
        {
            _gameFieldWidth = gameManager.gameFieldWidth - gameManager.headSize; // We subtract head size to ensure that whole head is visible in game scene
            _gameFieldHeight = gameManager.gameFieldHeight - gameManager.headSize;
            _gameFieldShiftInY = gameManager.gameFieldShiftInY;
            
            movementDirection = Vector2.zero; // At first the snake will not move
            // Start the coroutine to check for changes in movementDirection
            StartCoroutine(CheckForMovementChange());
        }

        private IEnumerator CheckForMovementChange()
        {
            // Wait until movementDirection is not zero
            while (movementDirection == Vector2.zero)
            {
                yield return null;
            }

            // Once movementDirection is not zero, start invoking Move
            InvokeRepeating(nameof(Move), gameManager.movementSpeed, gameManager.movementSpeed);
        }
        
        private void Move()
        {
            var currentTransform = transform;
            Vector2 currentPosition = currentTransform.position;
            currentPosition.x = (int)currentPosition.x;
            currentPosition.y = (int)currentPosition.y;

            // Calculate the new position based on the direction and head size
            var newPosition = currentPosition + movementDirection * gameManager.headSize;
            // Ensure position is not float because in speed sometime position becomes float and fails nokia snake feature
            newPosition.x = (int) newPosition.x;
            newPosition.y = (int) newPosition.y;

            // Update the new position
            currentTransform.position = newPosition;

            WrapAroundScreen();
            
            // Create and maintain the body parts
            gameManager.BodyManagement(currentPosition);

        }
        
        private void WrapAroundScreen()
        {
            var currentTransform = transform;
            // Check if the head has gone outside the right side of the screen
            if (transform.position.x > _gameFieldWidth / 2)
            {
                currentTransform.position = new Vector2(-_gameFieldWidth / 2, currentTransform.position.y);
                return;
            }
            // Check if the head has gone outside the left side of the screen
            if (transform.position.x < -_gameFieldWidth / 2)
            {
                currentTransform.position = new Vector2(_gameFieldWidth / 2, currentTransform.position.y);
                return;
            }

            // Check if the head has gone outside the top of the screen
            if (transform.position.y > (_gameFieldHeight / 2) + _gameFieldShiftInY)
            {
                currentTransform.position = new Vector2(currentTransform.position.x, -(_gameFieldHeight / 2) + _gameFieldShiftInY);
                return;
            }
            // Check if the head has gone outside the bottom of the screen
            if (transform.position.y < -(_gameFieldHeight / 2) + _gameFieldShiftInY)
            {
                currentTransform.position = new Vector2(currentTransform.position.x, (_gameFieldHeight / 2) + _gameFieldShiftInY);
            }
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            // Food?
            if (other.CompareTag(StringData.Food))
            {
                gameManager.score += gameManager.scoreIncreaseValue;
                gameManager.UpdateScore();
                _foodCount++;
                gameManager.foodEaten = true;
                // Remove the Food
                Destroy(other.gameObject);
                // Instantiate new Food on new random position
                gameManager.SpawnFood(StringData.Food);
                // After every 5 food one bonus will come
                if (_foodCount % 5 == 0) gameManager.SpawnFood(StringData.Bonus);
            }
            else if (other.CompareTag(StringData.Bonus))
            {
                gameManager.score += gameManager.bonusScore;
                gameManager.UpdateScore();
                gameManager.bonusEaten = true;
                // Remove the Bonus
                Destroy(other.gameObject);
            }
            // Collided with Body or Border
            else
            {
                Time.timeScale = 0;
                gameManager.GameOver();
            }
        }
    }
}
